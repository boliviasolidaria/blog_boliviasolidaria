'use strict';

const { development, test, production } = require('./config');

const configSeeder = {
  'development': {
    "dialect": development.db.dialect,
    "storage": development.db.storage,
    "debug": development.db.logging,
    "logging": development.db.logging
  },
  'test': {
    'dialect': test.db.dialect,
    "storage": test.db.storage,
    "debug": test.db.logging,
    "logging": test.db.logging
  },
  'production': {
    'dialect': production.db.dialect,
    "storage": production.db.storage,
    "debug": production.db.logging,
    "logging": production.db.logging
  }
};

module.exports = configSeeder;
