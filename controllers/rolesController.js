'use strict';

const ResponseFormat = require("../utils/responseFormat");
const ErrorResponse = require("../utils/errorResponse");
const validator = require('validator')

const debug = require('debug')('controller:roles');

module.exports = function setupRolesController(logs, models) {
  const { roles } = models;
  async function getRoles(req, res, next) {
    try {
      const _roles = await roles.findAndCountAll();
      return res.send(new ResponseFormat(true, _roles, "getting Roles successfully"));
    } catch (error) {
      next(error)
    }
  }

  async function getRole(req, res, next) {
    try {
      const foundRole = await roles.findOne({ where: { id: req.params.id } })
      if (!foundRole) {
        // return next(new ErrorResponse('Rol no ha sido encontrado', 404)) // otra forma
        throw new ErrorResponse('Rol no ha sido encontrado', 404);
      }
      res.send(new ResponseFormat(true, foundRole, "Se obtuvo el role satisfactoriamente!."))
    } catch (error) {
      next(error);
    }
  }

  async function createRole(req, res, next) {
    // !TODO validate uppper-lower CASE and "    "->sanitizador
    try {
      const data = req.body;
      if (!data.name) {
        throw new Error('El campo name es requerido');
      }
      const foundRole = await roles.findOne({ where: { name: data.name } })
      if (foundRole) {
        throw new Error('Ya existe el rol.')
      }
      const newRole = await roles.create({ name: data.name });
      res.send(new ResponseFormat(true, newRole, "Se creo el rol satisfactoriamente"));
    } catch (error) {
      next(error)
    }
  }

  async function updateRole(req, res, next) {
    try {
      const data = req.body;
      if (!data.name) {
        throw new Error('El campo name es requerido');
      }
      const foundRole = await roles.findOne({ where: { id: req.params.id } })
      foundRole.name = data.name;
      await foundRole.save();
      res.send(new ResponseFormat(true, foundRole, "Se actualizo el rol satisfactoriamente!."))
    } catch (error) {
      next(error)
    }
  }

  async function removeRole(req, res, next) {
    try {
      const foundRole = await roles.findOne({ where: { id: req.params.id } })
      if (!foundRole) {
        // return next(new ErrorResponse('Rol no ha sido encontrado', 404)) // otra forma
        throw new ErrorResponse('Rol no ha sido encontrado', 404);
      }
      // soft delete, eliminar en cascada...?
      await foundRole.destroy();
      res.send(new ResponseFormat(true, {}, "Se elimino el rol satisfactoriamente!."))
    } catch (error) {
      next(error);
    }

  }

  return {
    getRole,
    getRoles,
    createRole,
    updateRole,
    removeRole
  };
}
