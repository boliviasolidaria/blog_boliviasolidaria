'use strict';

const debug = require('debug')('controller:posts');

module.exports = function setupPostController(logs, models) {

  async function listarPosts (req, res, next) {
    // codigo aqui
    // ..
    return res.send({ ok: true });
  }

  async function crearPost (req, res, next) {
    // codigo aqui
    // ..
    return res.send({ ok: true });
  }

  async function modificarPost (req, res, next) {
    // codigo aqui
    // ..
    return res.send({ modificado: true });
  }

  async function borrarPost (req, res, next) {
    // codigo aqui
    // ..
    return res.send({ borrado: true });
  }
  return {
    listarPosts,
    crearPost,
    modificarPost,
    borrarPost
  };
}
