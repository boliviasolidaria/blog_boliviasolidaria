'use strict';

const chalk = require('chalk');
const debug = require('debug');
const path = require('path');
const { loadControllers } = require('../utils');

module.exports = function setupControllers (logs, models) {
  debug('Iniciando Controllers');

  const controllers = loadControllers(path.join(__dirname), {
    exclude: ['index.js'],
    excludeRegex: [/[~|#]$/, /^(.#)/]
  }, logs, models);

  logs.info('Cargados contraladores.');
  return controllers;
};
