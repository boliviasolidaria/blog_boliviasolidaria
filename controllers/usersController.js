'use strict';

const debug = require('debug')('controller:posts');
const userAndRoles = require('../utils/userAndRoles');

module.exports = function setupUserController(logs, models) {

  const { users, roles, role_users, posts, comments } = models;

  async function listarUsuarios(req, res, next) {

    // Special methods/mixins added to instances
    let one = await users.findOne({ where: { id: 2 } });
    // let result = await one.getPosts();
    let usuarios = await users.findAndCountAll({});
    
    // console.log('result: ', usuarios );
    // debug(result);
    return res.send({ ok: true, usuarios });

  }

  async function registrarUsuario(req, res, next) {
    // TODO: importante usar toLowerCase() y trim() en email
    // ..
    return res.send({ ok: true });
  }

  async function userLogin(req, res, next) {
    let resp = await userAndRoles(models ,logs).userLogin(req);
    if (resp) {
      // TODO: retornar a pagina principal
      // ..
      res.send({ ok: true });
    }
    // TODO: retornar a pagina de login e indicar error
    return res.status(401).send({ error: 'Error al ingresar, compruebe usuario y password' });
  }

  async function sessionInfo (req, res, next) {
    const userSession = await userAndRoles(models, logs).getUserSession(req);
    if (!userSession) {
      return res.status(401).send({ error: 'No session found' });
    }
    return res.status(200).send({ user:
                                  {
                                    email: userSession.dataValues.email,
                                    countLogin: userSession.dataValues.countLogin
                                  }
                                });
  };

  /** 
   * Checks if user doing request has given `role' permissions, if so passes to next controller, 
   * if not the request returns with code 401
   * @param {Object} req: Express request object
   * @param {Object} res: Express response object
   * @param {Object} next: Express next object
   * @param {String} role: String with role being checked, example: 'admin'
   */
  function hasPermission (role) {
    function _middleware (req, res, next) {
      userAndRoles(models, logs).checkSessionAllowedRole(req, role)
        .then(allowed => {
          if (!allowed) {
            res.status(401).send({ error: 'Not allowed session as requested role' });
          }
          next();
        })
        .catch(error => {
          logs.error(`Error checking permissions: ${error}`);
          console.log(`Error checking permissions: ${error}`);
          res.status(500).send({ error: 'Internal error checking permissions' });
        });
    };
    if (role) {
      return _middleware;
    }
    return;
  };

  return {
    listarUsuarios,
    registrarUsuario,
    userLogin,
    sessionInfo,
    hasPermission
  };
}
