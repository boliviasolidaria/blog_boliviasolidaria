# Blog Boliviasolidaria.org.bo

Aplicación web para el blog de boliviasolidaria.org.bo
 
Licencia: [AGPL v3](LICENSE.md)

## Tecnologías usadas

- Node.js version 12.16 recomendada.

## Instalación modo desarrollo

1. Descargar el proyecto, en la carpeta del proyecto descargado asegurarse de estar usando node.js 12.
2. Instalar las dependencias con `npm install`.
3. Copiar archivo de configuración `config/config.js.sample` como `config/config.js`.
4. Crear las tablas y relaciones en la Base de datos (sqlite3): `npm run setup`
5. Ejecutar en modo desarrollo: `npm run dev`.
6.  (opcional) Ejecutar los tests unitarios `npm run test`.

### Sobre los tests

#### Tests varios

Existen dos tipos de pruebas unitarias, las pruebas varias están en `tests/` y siempre empiezan con `misc_`, para ejecutarlas se usa `npm run test`.

#### Tests para endpoints

Las pruebas para los **endpoints** requieren una instancia de express en modo test, se puede hacer abriendo dos terminales y ejecutando:

1. `npm run testapp` (en una terminal)
2. `npm run test-endpoints` (en otra terminal)

Estos tests están en la carpeta `tests/` y siempre empiezan con `endpoint_`.

### TODO list

La lista de pendientes está en el [kanban](https://tree.taiga.io/project/nicovw-bolivia-solidaria-dev/kanban) las que inician con la palabra **blog**.
