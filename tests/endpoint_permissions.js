/* This file is part of blog_boliviasolidaria 
 * copyright Rodrigo Garcia 2020 <strysg@riseup.net>
 */
'use strict';

const test = require('ava');
const setup = require('../utils/setupTests');
const { userAndrRoles } = require('../utils/userAndRoles');
const axios = require('axios');
const appPort = 3091;

let db = null;
test.before(async t => {
  if (!db) {
    db = await setup.setupTestDatabase();
  }
});

test.serial('permissions #login - endpoint', async t => {
  const body = { email: 'pepito@example.com',  password: 'mi-secretito' };
  const config = {
    withCredentials: true,
    headers: {
      'Content-Type': 'application/json',
      'Accept-Encoding': '*/*',
      'User-Agent': 'Axios',
    }
  };
  let resp = await axios.post(`http://127.0.0.1:${appPort}/api/users/login`, JSON.stringify(body), config);
  t.truthy(resp.data.ok === true, 'Credenciales aceptadas');
  t.truthy(resp.status === 200, 'Status 200 Ok');
  t.truthy(Array.isArray(resp.headers['set-cookie']) === true, 'set-cookie array');
  t.truthy(resp.config.xsrfCookieName === 'XSRF-TOKEN', 'xsrfCookieName');
  // getting session cookie
  // test.sessionCookie = resp.headers['set-cookie'][0].split('.')[0];
  test.sessionCookie = resp.headers['set-cookie'][0];
});

test.serial('permissions #checkSessionAllowed Role - Not allowed', async t => {
  const body = { email: 'pepito@example.com',  password: 'insecreto' };
  const config = {
    headers: {
      'Content-Type': 'application/json',
      'Accept-Encoding': '*/*',
      'User-Agent': 'Axios',
    }
  };
  try {
    let resp = await axios.post(`http://127.0.0.1:${appPort}/api/users/login`, body, config);
    t.truthy(resp.data.error !== '', 'Rechazando credenciales incorrectas');
    t.truthy(resp.status !== 200, 'Status distinto de 200');
  } catch (e) {
    // console.error('Error ', e);
    t.truthy(e.response.data.error !== '', 'Rechazando credenciales incorrectas');
    t.truthy(e.response.status !== 200, 'Status distinto de 200');
  }
});

test.serial('permissions #user from session cookie - get user', async t => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
      'Cookie': test.sessionCookie
    }
  };
  // getting user from cookie session
  let resp = await axios.get(`http://127.0.0.1:${appPort}/api/users/session`, config);
  t.truthy(resp.status === 200, 'status 200');
  t.truthy(resp.data.user !== undefined, 'Objeto usuario obtenido');
  t.truthy(resp.data.user.email === 'pepito@example.com', 'email usuario');
  t.truthy(resp.data.user.countLogin > 0, 'logins');
});

test.serial('permissions #user from invalid cookie - get user', async t => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
      'Cookie': 'shckef_cookie0'+test.sessionCookie
    }
  };
  try {
    let resp = await axios.get(`http://127.0.0.1:${appPort}/api/users/session`, config);
    t.truthy(resp.status === 401, 'staus 401');
  } catch (e) {
    t.truthy(e.response.status === 401, 'staus 401');
    t.truthy(e.response.data.error !== '', 'Error session');
    t.truthy(e.response.data.user === undefined, 'Ningun usuario devuelto');
  }
});

test.serial('permissions #list users checking logged in user belongs to "admin" role', async t => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
      'Cookie': test.sessionCookie
    }
  };
  let resp = await axios.get(`http://127.0.0.1:${appPort}/api/users/list`, config);
  t.truthy(resp.data.ok === true, 'Comprobados permisos');
  t.truthy(resp.data.usuarios.count > 0, 'Devolviendo listando de usuarios');
});

test.serial('permissions #list users checking incorrect user has not "admin" role', async t => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
      'Cookie': 'cuakuiqwre sfdajs;ldf'
    }
  };
  let response;
  try {
    let resp = await axios.get(`http://127.0.0.1:${appPort}/api/users/list`, config);
    response = resp;
  } catch (e) {
    response = e.response;
  }
  t.truthy(response.data.ok !== true, 'Comprobados permisos');
  t.truthy(response.status === 401, 'Comprobado status 401 (sin permisos)');
});
