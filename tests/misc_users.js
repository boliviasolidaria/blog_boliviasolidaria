/* This file is part of blog_boliviasolidaria 
 * copyright Rodrigo Garcia 2020 <strysg@riseup.net>
 */
'use strict';

const test = require('ava');
const setup = require('../utils/setupTests');

let db = null;

test.before(async () => {
  
  if (!db) {
    db = await setup.setupTestDatabase();
  }
});

test.serial('users#findAll', async t => {
  const { users } = db.models;

  let resp = await users.findAll({});
  t.true(resp.length > 1, 'Mas de un registro en la BD');
});

test.serial('users # create and delete', async t => {
  const { users } = db.models;
  let obj = {
    name: 'Repito',
    last_name: 'Rerez',
    email: 'repito@example.com',
    password: 'cron',
    cellphone: '12345678',
    city: 'La Paz',
  };
  let user = await users.create(obj);
  console.log('user.name', user.name, user.id, user.email);
  t.truthy(user.name === 'Repito', 'comprobando creacion usuario - name');
  t.truthy(user.id > 0, 'comprobando creacion usuario - id');
  t.truthy(user.email === 'repito@example.com', 'comprobando creacion usuario - email');

  // borrando registro creado
  let deleted = await users.destroy({ where: { id: user.id }});
  t.is(deleted, 1, 'Borrado de registro de usuario en BD');
});

test.serial('users # filter', async t => {
  const { users } = db.models;
  
  let resp = await users.findAll({ where: { email: 'pepito@example.com' }});
  t.truthy(resp[0].dataValues.email === 'pepito@example.com', 'Comprbando email');
});

// colocar mas tests aqui
// ver ejemplos: https://gitlab.softwarelibre.gob.bo/agetic/agetic-aplicacion-base/base-backend/-/tree/master/src%2Finfrastructure%2Ftests

