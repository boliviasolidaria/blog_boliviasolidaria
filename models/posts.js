'use strict';

const util = require('./util');
const moment = require('moment');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    title: {
      type: DataTypes.STRING(500),
      allowNull: false
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    date: {
      type: DataTypes.DATE,
      defaultValue: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
    },
    category: {
      type: DataTypes.STRING(300),
      allowNull: false,
      defaultValue: '-'
    },
    status: {
      type: DataTypes.ENUM,
      values: ['wrote', 'checked', 'published']
    },
  };

  // fields = util.setTimestamps(fields);

  const post = sequelize.define('posts', fields, {
    timestamps: false,
    tableName: 'posts'
  });

  // const post = sequelize.define('post', {
  //   title: DataTypes.STRING,
  //   content: DataTypes.TEXT,
  //   date: DataTypes.DATE,
  //   category: DataTypes.STRING,
  //   status: {
  //     type: DataTypes.ENUM,
  //     values: ['wrote', 'checked', 'published']
  //   },
  //   id_user: DataTypes.INTEGER
  // }, {});

  // post.associate = function (models) {
  //   // associations can be defined here
  //   post.belongsTo(models.user, {
  //     foreignKey: 'id_user'
  //   });
  //   user.hasMany(models.post, {
  //     foreignKey: 'id_user'
  //   });
  // };

  return post;
};
