'use strict';

module.exports = function associations(models) {
  const {
    users,
    roles,
    role_users,
    posts,
    comments
  } = models;

  // usuarios y roles
  // user.belongsTo(role_user, { foreignKey: { name: 'id_user', allowNull: false }, as: 'user' });
  // role_user.hasMany(user, { foreignKey: { name: 'id_user', allowNull: true }, as: 'user' });

  // role.belongsTo(role_user, { foreignKey: { name: 'id_role', allowNull: false }, as: 'role' });
  // role_user.hasMany(role, { foreignKey: { name: 'id_role', allowNull: true }, as: 'role' });

  // --- user - role ----
  // !! TODO revisar las columnas extra => date_assigned, _user, ...
  users.belongsToMany(roles, {
    through: {
      model: role_users
    },
    as: 'roles',
    foreignKey: 'id_user'
  });
  // role - user
  roles.belongsToMany(users, {
    through: {
      model: role_users,
    },
    as: 'users',
    foreignKey: 'id_role'
  });

  // --- user - comment ----
  users.hasMany(comments, {
    foreignKey: 'id_user'
  });
  // comment - user
  comments.belongsTo(users, {
    foreignKey: 'id_user'
  });

  // --- post - user ----
  posts.belongsTo(users, {
    foreignKey: 'id_user'
  });
  // --- user - post ---
  users.hasMany(posts, {
    foreignKey: 'id_user'
  });

  // --- post - comment ---
  posts.hasMany(comments, {
    foreignKey: 'id_post'
  });
  // comment - post
  comments.belongsTo(posts, {
    foreignKey: 'id_post'
  });

  return models;
};
