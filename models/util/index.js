const Sequelize = require('sequelize');
const fs = require('fs');
const path = require('path');
const moment = require('moment');

const pk = {
  primaryKey: true,
  autoIncrement: true,
  type: Sequelize.INTEGER,
  xlabel: 'ID'
};

const timestamps = {
  _user_created: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  _user_updated: {
    type: Sequelize.INTEGER,
  },
  _created_at: {
    type: Sequelize.STRING(50),
    allowNull: false,
    defaultValue: moment().format('YYYY-MM-DD HH:mm:ss.SSS')
  },
  _updated_at: { // revisar 
    type: Sequelize.STRING(50),
    defaultValue: moment().format('YYYY-MM-DD HH:mm:ss.SSS')
  }
};

function setTimestamps (fields) {
  return Object.assign(fields, timestamps);
}

module.exports = {
  pk,
  setTimestamps
};
