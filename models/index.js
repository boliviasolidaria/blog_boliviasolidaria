'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const chalk = require('chalk');

const arrays = require('../utils/arrays');
const associations = require('./associations.js');

const db = {};

async function inicializarBD(sequelize, config, logs) {
  // probando conexion
  try {
    sequelize.authenticate();
  } catch (e) {
    console.error(chalk.red('Error al conectarse a la BD:'));
    console.log(e, '\n');
    logs.error('Error al conectarse a la BD' + e);
    throw new Error('Error conectandose a la BD:', e);
  }
  // cargando modelos
  let _models = {};
  console.log(chalk.blueBright('* Loading models from models/index.js:'));

  const PATH = path.join(__dirname);
  let files = fs.readdirSync(PATH);
  let models = {};

  // ignorar associations.js como modelo
  arrays.removeAll(['associations.js','index.js'], files);

  // excluyendo archivos expresiones regulares (temporales del editor emacs por ejemplo)
  let excluir = [];
  [/[~|#]$/, /^(.#)/].map((re) => {
    let regExp = new RegExp(re);
    files.map((file) => {
      if (regExp.test(file)) {
        excluir.push(file);
      }
    });
  });
  if (excluir.length > 0) {
    arrays.removeAll(excluir, files);
  }
  
  files.forEach(function (file) {
    let pathFile = path.join(PATH, file);
    try {
      if (fs.statSync(pathFile).isDirectory() === false) {
        file = file.replace('.js', '');
        _models[file] = sequelize.import(pathFile);
        console.log(chalk.blueBright(`  ⛀  ${file}`));
        // console.log('MODEL:', file,'---\n', typeof _models[file]);
      }
    } catch (e) {
      console.error(chalk.red('Error ocurrido leyendo archivo', pathFile, '\n:', e));
      logs.error('Error ocurrido leyendo archivo', pathFile, ':', e);
    }

  });

  // cargando asociaciones
  try {
    models = associations(_models);
    console.log(chalk.green('✓ Asociaciones cargadas exitosamente.\n'));
  } catch (e) {
    console.error(chalk.red('✕ Error cargando asociaciones'));
    console.error(e + '\n');
    logs.error('✕ Error cargando asociaciones: ', e);
  }
  db.models = models;
  db.sequelize = sequelize;
  db.Sequelize = Sequelize;
  // console.log(':::::\n', db.models, '....');
  return db;
}


module.exports = {
  inicializarBD
};
