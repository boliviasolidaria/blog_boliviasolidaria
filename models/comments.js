'use strict';

const util = require('./util');
const moment = require('moment');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    content: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    date: {
      type: DataTypes.DATE,
      defaultValue: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
    },
    name_author: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    email_author: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    status: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    id_post: {
      type: DataTypes.INTEGER
    },
    id_user: {
      type: DataTypes.INTEGER
    }
  };

  // fields = util.setTimestamps(fields);

  let comment = sequelize.define('comments', fields, {
    timestamps: false,
    tableName: 'comments'
  });

  return comment;
};
