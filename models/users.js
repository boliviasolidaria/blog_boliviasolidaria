'use strict';
const bcrypt = require('bcrypt');
const util = require('./util');
const moment = require('moment');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    name: {
      type: DataTypes.STRING(300), // de hasta 300 de longitud
      allowNull: false,
      defaultValue: '-'
    },
    last_name: {
      type: DataTypes.STRING(300), // de hasta 300 de longitud
      allowNull: false,
      defaultValue: '-'
    },
    email: {
      type: DataTypes.STRING(300),
      allowNull: false,
      defaultValue: '-',
      unique: true
    },
    password: {
      type: DataTypes.STRING(500),
      allowNull: false,
      defaultValue: '-'
    },
    status: {
      type: DataTypes.ENUM,
      values: ['active', 'inactive'],
      defaultValue: 'active'
    },
    cellphone: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: '-'
    },
    city: {
      type: DataTypes.STRING(500),
      allowNull: false,
      defaultValue: '-'
    },
    country: {
      type: DataTypes.STRING(100),
      allowNull: false,
      defaultValue: '-'
    },
    countLogin: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.STRING(20),
      defaultValue: moment().format('YYYY-MM-DD HH:mm:ss.SSS')
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.STRING(20),
      defaultValue: '-'
    }
  };

  // opciones y hooks que se ejecutan automaticamente
  const options = {
    timestamps: false,
    tableName: 'users',
    hooks: {
      beforeCreate: (user) => {
        const salt = bcrypt.genSaltSync(10);
        user.password = bcrypt.hashSync(user.password, salt);
      }
    }
  };

  // agregando campos para auditoria
  // fields = util.setTimestamps(fields);

  const User = sequelize.define('users', fields, options);

  // fix source sequelize v5: https://teamtreehouse.com/library/instance-methods
  User.prototype.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
  };

  User.prototype.belongsToRole = async function (user_id, allowedRole) {
    const query = `
        SELECT ru.id_user, ru.id_role 
           FROM role_users as ru, users as u, roles as r
	   WHERE ru.id_user = u.id AND ru.id_role = r.id
	       AND r.name = :allowedRole AND u.id = :user_id`;
    try {
      return await sequelize.query(query, {
        replacements: {
          allowedRole,
          user_id
        },
        type: sequelize.QueryTypes.SELECT
      });
    } catch (e) {
      throw (e);
    }
  };
  

  
  return User;
};
