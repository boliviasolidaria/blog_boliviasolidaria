'use strict';

const util = require('./util');
const moment = require('moment');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    date_assigned: {
      type: DataTypes.STRING(50),
      defaultValue: moment().format('YYYY-MM-DD HH:mm:ss.SSS'), // la fecha actual en formato (YYYY-MM-DD HH:mm:ss.SSS
    },
    id_role: {
      type: DataTypes.INTEGER
    },
    id_user: {
      type: DataTypes.INTEGER
    },
    _user: {
      type: DataTypes.STRING(100)
    }
  };

  // fields = util.setTimestamps(fields);

  const role_user = sequelize.define('role_users', fields,{
    timestamps: false,
    tableName: 'role_users'
  });

  return role_user;
};
