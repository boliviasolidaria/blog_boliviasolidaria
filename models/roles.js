'use strict';

const util = require('./util');
const moment = require('moment');
const { Op } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    name: {
      type: DataTypes.STRING(300), // de hasta 300 de longitud
      allowNull: false,
      defaultValue: '-'
    },
  };

  // agregando campos para auditoria
  // fields = util.setTimestamps(fields);

  let role = sequelize.define('roles', fields, {
    timestamps: false,
    tableName: 'roles'
  });

  return role;
};
