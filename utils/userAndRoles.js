/* This file is part of blog_boliviasolidaria 
 * copyright Rodrigo Garcia 2020 <strysg@riseup.net>
 */
'use strict';

const session = require('express-session');
const debug = require('debug')('utils/userAndRoles ');
const Sequelize = require('sequelize');

module.exports = function (models, logs, sequelize) {
  /**
   * Checks wheter the given request has a valid cookie session and returns the user with valid session
   * @param {Object} req: Express request object
   * @return {Object}: The logged in user or null if invalid session.
   */
  const getUserSession = async function (req) {
    const { users, roles } = models;
    if (!checkSession(req)) {
      return null;
    }
    try {
      let user = await users.findOne({ where: { email: req.session.user.email,
                                            id: req.session.user.id }});
      return user;
    } catch (e) {
      logs.error(`Error buscando usuario con id ${req.session.user.id}: ${e}`);
      return null;
    }
  };

  const checkSession = function (req) {
    return req.session.user && req.session.cookie;
  };

  /**
   * Gets the user from the fiven request and checks wheter the user belongs to the `allowedRoles'.
   * @param {Object} req: Express request object
   * @param {String} allowedRole: Role name, example: 'Admin'  'PostWriter'
   * @return {Boolean}: true if it belongs to all of them, false otherwise.
   */
  const checkSessionAllowedRole = async function (req, allowedRole) {
    const { users, roles, role_users } = models;
    const resp = await getUserSession(req);
    if (!resp) {
      return false;
    }
    const user = resp.dataValues;

    try {
      let result = await resp.belongsToRole(user.id, allowedRole);
      return result.length > 0;
    } catch (e) {
      console.log(`Error buscando usuario con pertenencia a rol ${allowedRole}: ${e}`);
      logs.error(`Error buscando usuario con pertenencia a rol ${allowedRole}: ${e}`);
      return false;
    }
  };

  /**
   * Logins a user given the email and password, uses express-session to create a cookie with user data to 
   * be checked later, also appends session_id into user table column session_ids.
   * @param {Object} req: express.js request, should contain in its body userEmail and password both strings.
   * @param {Object} session: express-session instance.
   * @return {Object}: If logged in successfully returns req.session.id (modified) if not returns null
   */
  const userLogin = async function (req) {
    const { email, password } = req.body;
    const { users } = models;

    let user = await users.findOne({ where: { email: email } });
    if (!user || user.validPassword(password) === false) {
      req.session.destroy(async function(err) {
        if (err) {
          logs.error(`Error destruyendo sesion: ${err}`);
        }
        console.log(`¡Sesion destruida!`);
      });
      return null;
    }
    // sesion valida
    user.countLogin++;
    req.session.user = {
      email: user.dataValues.email,
      id: user.dataValues.id
    };
    req.session.cookie.user_sid = req.sessionID;
    try {
      await users.update({ countLogin: user.countLogin }, { where: { id: user.id }});
    } catch (e) {
      logs.error('Error updating user count login', e);
    }
    return req.session;
  };

  return {
    getUserSession,
    checkSession,
    checkSessionAllowedRole,
    userLogin
  };
};
