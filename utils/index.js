/* This file is part of blog_boliviasolidaria 
 * copyright Rodrigo Garcia 2020 <strysg@riseup.net>
 */
'use strict';

const fs = require('fs');
const path = require('path');

const Response = require('./response');
const arrays = require('./arrays');

let res;
/**
 * Carga los archivos js en la ruta especificada
 *
 * @param {string} PATH: Path del directorio de donde se cargará los modelos del sistema
 * @param {object} opts: Objeto de configuración
 *                       exclude: una lista de archivos para ignorar
 *                       excludeRegex: lista de rexpresiones para ignorar nombres de archivos
 * @param {array} args: Lista de argumentos que se pasaran a los archivos que se carguen
 */
function loadJsFiles (PATH, opts = {}, ...args) {
  // if (!res) {
  //   res = Response(logs);
  // }
  let files = fs.readdirSync(PATH);
  let jsFiles = {};

  if (opts.exclude) {
    arrays.removeAll(opts.exclude, files);
  }

  // para excluir tambien expresiones regulares
  if (opts.excludeRegex) {
    let excluir = [];
    opts.excludeRegex.map((re) => {
      let regExp = new RegExp(re);
      files.map((file) => {
        if (regExp.test(file)) {
          excluir.push(file);
        }
      });
    });
    if (excluir.length > 0) {
      arrays.removeAll(excluir, files);
    }
  }

  files.forEach(function (file) {
    let pathFile = path.join(PATH, file);

    if (fs.statSync(pathFile).isDirectory()) {
      jsFiles = Object.assign(jsFiles, loadJsFiles(pathFile, opts, ...args));
    } else {
      file = file.replace('.js', '');
      jsFiles[file] = require(pathFile)(...args);
    }
  });

  return jsFiles;
}

function loadControllers (PATH, opts = {}, logs, models) {
  let args = [logs, models];
  return loadJsFiles(PATH, opts, ...args);
}

module.exports = {
  loadJsFiles,
  loadControllers,
};
