/* This file is part of blog_boliviasolidaria 
 * copyright Rodrigo Garcia 2020 <strysg@riseup.net>
 */
'use strict';
/**
 * Funciones para formatear respuestas y escribir en logs en caso de error.
 * Extraído de https://gitlab.softwarelibre.gob.bo/agetic/agetic-aplicacion-base/base-backend
 */
module.exports = function setupResponse (logs) {
  function success (data, message) {
    return {
      code: 0,
      data,
      message: message || 'OK'
    };
  }

  async function error (error) {
    let data = {
      code: -1,
      data: process.env.NODE_ENV !== 'production' ? error : '',
      message: error.message || 'ERROR DESCONOCIDO'
    };

    let tipo = 'ERROR GENERAL';
    if (error && error.SequelizeDatabaseError) {
      data.message = error.SequelizeDatabaseError;
      tipo = 'ERROR BD';
    }

    if (error.name && error.name === 'SequelizeDatabaseError') {
      tipo = 'ERROR BD';
    }

    await logs.error(data.message, tipo, error);
    return data;
  }

  function warning (error) {
    return {
      code: 1,
      data: error,
      message: error.message || 'ADVERTENCIA'
    };
  }

  return {
    success,
    error,
    warning
  };
};
