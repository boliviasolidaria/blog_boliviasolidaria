'use strict';

const config = require('../config/config.js')['test'];
var http = require('http');
const Sequelize = require('sequelize');
const pino = require('pino');
const cors = require('cors');

const logLevels = { production: 'error', development: 'debug', test: 'info' };
const logLevel = pino.levels.labels[pino.levels.values[logLevels['test']]];

const fileLogger = require('pino')(
  { level: logLevel },
  pino.destination({ dest: `./logs/test.log`, sync: false }));

const errorHandler = require('../middlewares/errorHandler');

async function setupTestApp (appPort = 3091) {
  // creates an express.js instance
  var express = require('express');
  var routes = require('../routes');
  var path = require('path');
  
  let app = express();
  app.disable('x-powered-by');
  // sessions
  var session = require("express-session");
  var SequelizeStore = require('connect-session-sequelize')(session.Store);
  var sequelizeForSessions = new Sequelize({ "dialect": "sqlite", "storage": "./database/session.test.sqlite" });

  var sessionStore = new SequelizeStore({
    db: sequelizeForSessions
  });
  app.use(session({
    secret: config.auth,
    resave: false,
    saveUninitialized: false,
    cookie: {
      secure: false,
      maxAge: 60000, // 60s
      name: 'bsession.sid'
    },
    store: sessionStore
  }));
  sessionStore.sync(); // initialize session database

  app.use(express.urlencoded({ extended: true }));
  app.use(express.static(path.join(__dirname, '../public')));
  app.use(cors());
  
  // database
  app.db = await setupTestDatabase();
  var controllers = require('../controllers')(fileLogger, app.db.models);

  app.use(express.json({ limit: '12mb' }));
  app.use(express.urlencoded({ extended: true }));
  app.use(express.static(path.join(__dirname, 'public')));
  app.use(errorHandler);
  app.set('port', appPort);

  app.use('/api', routes(controllers));

  // init server
  var server = http.createServer(app);
  server.listen(appPort);
  // server.on('error', onError);
  //server.on('listening', onListening);
  console.log(`\n** Aplicación en entorno test ejecutando, puerto ${app.get('port')} **\n`);
  return app;
}

async function setupTestDatabase () {
  const _db = require('../models');

  let sequelize;
  try {
    sequelize = new Sequelize(config.db);
  } catch (e) {
    console.error('Error BD:', e);
    throw new Error(e);
  }
  let db = await _db.inicializarBD(sequelize, config.db, fileLogger);
  await iniciarRegistros(db.sequelize, db.models);
  return db;
};


// registros de usuarios de pruebas
const bcrypt = require('bcrypt');

const salt = bcrypt.genSaltSync(10);
const password = 'mi-secretito';
const hash = bcrypt.hashSync('mi-secretito', salt);
const usersRegs = [
  {
    name: 'Pepito',
    last_name: 'Perez',
    email: 'pepito@example.com',
    password: hash,
    cellphone: '12345678',
    city: 'La Paz',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    name: 'patty',
    last_name: 'Perez',
    email: 'patty@example.com',
    password: hash,
    cellphone: '1234d78',
    city: 'La Paz',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    name: 'Juanita',
    last_name: 'Ramirez',
    email: 'juanita@example.com',
    password: hash,
    cellphone: '87654321',
    city: 'Tarija',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    name: 'Jose',
    last_name: 'Valderrama',
    email: 'jose@example.com',
    cellphone: '8778374',
    password: hash,
    city: 'Chuquisaca',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
];

const rolesRegs = [
  {
    name: 'Admin',
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    name: 'Post Writer',
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    name: 'Post Checker',
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    name: 'Post Publisher',
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    name: 'Comment Checker',
    createdAt: new Date(),
    updatedAt: new Date()
  }
];

const roleUserRegs = [
  {
    id_user: 1,
    id_role: 1,
    date_assigned: new Date(),
    _user: 1,
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    id_user: 2,
    id_role: 5,
    date_assigned: new Date(),
    _user: 1,
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    id_user: 1,
    id_role: 2,
    date_assigned: new Date(),
    _user: 1,
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    id_user: 4,
    id_role: 2,
    date_assigned: new Date(),
    _user: 1,
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    id_user: 3,
    id_role: 3,
    date_assigned: new Date(),
    _user: 1,
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    id_user: 2,
    id_role: 2,
    date_assigned: new Date(),
    _user: 1,
    createdAt: new Date(),
    updatedAt: new Date(),
  },
];

async function iniciarRegistros (sequelize, models) {
  const { users, roles, role_users } = models;

  try {
    let resp = await users.findAndCountAll({});
    if (resp.count > 0) {
      return; // already created
    }
  } catch (e) {
    await sequelize.sync({ force: true });
  }

  await users.truncate({ cascade: true });
  await roles.truncate({ cascade: true });
  await role_users.truncate({ cascade: true });
  console.log('¡Registros borrados!');
  try {
    await roles.bulkCreate(rolesRegs);
    await users.bulkCreate(usersRegs);
    await role_users.bulkCreate(roleUserRegs);
    console.log('Registros de prueba en BD insertados.');
  } catch (e) {
    console.error('Error insertando registros', e);
    throw new Error(e);
  }

};

module.exports = {
  setupTestApp,
  setupTestDatabase
};
