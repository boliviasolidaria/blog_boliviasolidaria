class ResponseFormat {
     /**
   * @param {boolean} success status of response
   * @param {*} data data response
   * @param {string} message additional message of response
   */
    constructor(success, data, message) {
        this.success = success
        this.message = message || ''
        this.data = data
    }
}

module.exports = ResponseFormat