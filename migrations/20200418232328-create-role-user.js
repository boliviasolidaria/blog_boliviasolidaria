'use strict';
const moment = require('moment');
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('role_users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      id_user: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'users',
          key: 'id',
          as: 'users'
        }
      },
      id_role: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'roles',
          key: 'id',
          as: 'roles'
        }
      },
      date_assigned: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      _user: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.STRING(20),
        defaultValue: moment().format('YYYY-MM-DD HH:mm:ss.SSS')
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.STRING(20),
        defaultValue: '-'
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('role_users');
  }
};
