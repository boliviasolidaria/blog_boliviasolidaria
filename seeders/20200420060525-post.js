'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
    */
    const posts = [
      {
        title: 'Corona virus reportes',
        content: 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.',
        date: new Date(),
        category: 'Salud',
        status: 'wrote',
        id_user: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: 'Corona virus2 reportes',
        content: 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto2.',
        date: new Date(),
        category: 'Salud',
        status: 'wrote',
        id_user: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: 'Clasico Paceno',
        content: 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.',
        date: new Date(),
        category: 'Deportes',
        status: 'checked',
        id_user: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: 'Concurso de robotica de Bolivia',
        content: 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.',
        date: new Date(),
        category: 'Educacion',
        status: 'published',
        id_user: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },

    ]
    return queryInterface.bulkInsert('posts', posts, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
