'use strict';
const bcrypt = require('bcrypt');

module.exports = {
  up: (queryInterface, Sequelize) => {
    const salt = bcrypt.genSaltSync(10);
    const password = 'mi-secretito';
    const hash = bcrypt.hashSync('mi-secretito', salt);
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
    */
    const users = [
      {
        name: 'Pepito',
        last_name: 'Perez',
        email: 'pepito@example.com',
        password: hash,
        cellphone: '12345678',
        city: 'La Paz',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
      name: 'patty',
        last_name: 'Perez',
        email: 'patty@example.com',
        password: hash,
        cellphone: '1234d78',
        city: 'La Paz',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Juanita',
        last_name: 'Ramirez',
        email: 'juanita@example.com',
        password: hash,
        cellphone: '87654321',
        city: 'Tarija',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Jose',
        last_name: 'Valderrama',
        email: 'jose@example.com',
        password: hash,
        cellphone: '8778374',
        city: 'Chuquisaca',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Carlita',
        last_name: 'hdhdh',
        email: 'carlita@example.com',
        password: hash,
        cellphone: '8776734',
        city: 'Chuquisaca',
        createdAt: new Date(),
        updatedAt: new Date(),
      }
    ];

    return queryInterface.bulkInsert('users', users, {});

  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};

