'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
    */
    const roles = [
      {
        name: 'Admin',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Post Writer',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Post Checker',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Post Publisher',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Comment Checker',
        createdAt: new Date(),
        updatedAt: new Date()
      }]

    return queryInterface.bulkInsert('roles', roles, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
    */
    return queryInterface.bulkDelete('roles', null, {});
  }
};
