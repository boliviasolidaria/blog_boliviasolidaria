'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.
      Example:
    */
    const roleUser = [
      {
        id_user: 2,
        id_role: 5,
        date_assigned: new Date(),
        _user: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id_user: 1,
        id_role: 2,
        date_assigned: new Date(),
        _user: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id_user: 4,
        id_role: 2,
        date_assigned: new Date(),
        _user: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id_user: 3,
        id_role: 3,
        date_assigned: new Date(),
        _user: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id_user: 2,
        id_role: 2,
        date_assigned: new Date(),
        _user: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]
    return queryInterface.bulkInsert('role_users', roleUser, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
