'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
    */
    const comments = [
      {
        content: 'Muy buen reporte y muy informativo!',
        date: new Date(),
        name_author: 'jose',
        email_author: 'jose@example.com',
        status: true,
        id_post: 3,
        id_user: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        content: 'Excelente post!',
        date: new Date(),
        name_author: 'juanita',
        email_author: 'juanita@example.com',
        status: true,
        id_post: 2,
        id_user: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]
    return queryInterface.bulkInsert('comments', comments, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
