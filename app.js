/* This file is part of blog_boliviasolidaria 
 * copyright Rodrigo Garcia 2020 <strysg@riseup.net>
 */
var express = require('express');

var path = require('path');
var cookieParser = require('cookie-parser');
var chalk = require('chalk');

const Sequelize = require('sequelize');
const env = process.env.NODE_ENV || 'development';
const config = require('./config/config.js')[env];
// importing middleware
const errorHandler = require('./middlewares/errorHandler');

var sequelize;

var app = express();
// To remove x-powered-by on headers
app.disable('x-powered-by');

// to set log level
var pino = require('pino');
const logLevels = { production: 'error', development: 'debug', test: 'info' };
const logLevel = pino.levels.labels[pino.levels.values[logLevels[env]]];

// Two loggers
// - pinoHttp default logger for express http requests
// - fileLogger general use logger that writes to file
var pinoHttp = require('pino-http')({ level: logLevel });
const fileLogger = require('pino')(
  { level: logLevel },
  pino.destination({ dest: `./logs/${env}.log`, sync: false }));

var db = require('./models');

// session and cookies
var session = require("express-session");
var SequelizeStore = require('connect-session-sequelize')(session.Store);
var sequelizeForSessions = new Sequelize({ "dialect": "sqlite", "storage": "./database/session.sqlite" });

// functions
function iniciarLogs(app) {
  app.use(pinoHttp);

  // pinoHttp.level(pino.levels.values[logLevels[env]]);
  //fileLogger.level(pino.levels.values[logLevels[env]]);
  fileLogger[logLevel]('Logs iniciados: ' + new Date());
}

async function iniciarBD() {
  if (config.use_env_variable) {
    sequelize = new Sequelize(process.env[config.use_env_variable], config);
  } else {
    sequelize = new Sequelize(config.db);
  }

  try {
    app.db = await db.inicializarBD(sequelize, config);
  } catch (e) {
    process.exit(1);
  }
}

function configurarExpress(routes, controllers) {

  try {
    iniciarLogs(app);
  } catch (e) {
    console.error(chalk.red('Error iniciando modulo logs'), e);
  }

  // cookie para autenticacion
  try {
    var sessionStore = new SequelizeStore({
      db: sequelizeForSessions
    });
    app.use(session({
      secret: config.auth,
      resave: false,
      proxy: true, // TODO: check this if you do SSL outside of node.
      saveUninitialized: false,
      cookie: {
        secure: env === 'production' ? true : false,
        maxAge: 3000000,
        name: 'bsession.sid'
      },
      store: sessionStore
    }));

    sessionStore.sync(); // initialize session database
  } catch (e) {
    console.error(chalk.red('Error iniciando express-session:\n', e));
  }
  
  try {
    app.use(express.json({ limit: '12Mb' }));
    app.use(express.urlencoded({ extended: false }));
    app.use(express.static(path.join(__dirname, 'public')));

    app.use('/api', routes(controllers));

    // errorHandler middleware 
    app.use(errorHandler);

  } catch (e) {
    console.error(chalk.red('Error iniciando express:', e));
  }
}

async function initApp() {

  await iniciarBD();
  // console.log('app.db.models:', app.db.models);

  var controllers = require('./controllers')(fileLogger, app.db.models);
  var routes = require('./routes');

  configurarExpress(routes, controllers);

  console.log('\n', chalk.bold.magenta(' Aplicación'),
    chalk.bold.greenBright('blog_boliviasolidaria'),
    chalk.magenta('iniciada\n'));
  console.log('     ,=""=,');
  console.log('    c , _,}');
  console.log('    /\\  @ )                 __');
  console.log("   /  ^~~^\\          <=.,__/ '}=");
  console.log("  (_/ ,, ,,)          \\_ _>_/~");
  console.log("   ~\\_(/-\\)'-,_,_,_,-'(_)-(_)  -Naughty");
  console.log('\nAplicación en entorno ', chalk.bgBlack(chalk.bold.magenta(env)), ` lista.\n`);

  return app;
}

module.exports = {
  initApp
};
