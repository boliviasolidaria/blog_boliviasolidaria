var express = require('express');
const fs = require('fs');
const path = require('path');
const debug = require('debug');
const chalk = require('chalk');
var api = express.Router();

const arrays = require('../utils/arrays');

module.exports = function setupRoutes(controllers) {
  const { postsController, usersController, rolesController } = controllers;

  api.get('/', (req, res, next) => { res.send('api root'); });

  // cargando rutas para posts
  api.get('/posts/list', postsController.listarPosts);
  api.post('/posts/create', postsController.crearPost);
  api.patch('/posts/modify/:id', postsController.modificarPost);
  api.delete('/posts/delete/:id', postsController.borrarPost);
  // roles routes
  api
    .route('/roles')
    .get(rolesController.getRoles)
    .post(rolesController.createRole);
  api
    .route('/roles/:id')
    .get(rolesController.getRole)
    .put(rolesController.updateRole)
    .delete(rolesController.removeRole);


  // cargando rutas para usuarios
  api.get('/users/list', usersController.hasPermission('Admin'), usersController.listarUsuarios);
  api.post('/users/login', usersController.userLogin);

  api.get('/users/session', usersController.sessionInfo);
  // continuar
  // ..

  
  console.log(chalk.blueBright('* ENDPOINTS loaded from routes/index.js:'));
  api.stack.forEach(r => {
    let method = '';
    if (r.route.methods.get) {
      method += chalk.cyan('GET ');
    }
    if (r.route.methods.post) {
      method += chalk.yellowBright('POST ');
    }
    if (r.route.methods.patch) {
      method += chalk.blue('PATCH ');
    }
    if (r.route.methods.put) {
      method += chalk.blueBright('PUT ');
    }
    if (r.route.methods.delete) {
      method += chalk.redBright('DELETE ');
    }
    console.log(`  ⮱ [${method}] ` + chalk.yellow(r.route.path));
  });
  return api;
};
